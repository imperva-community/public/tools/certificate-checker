# Imperva Cloud WAF Certificate Checker
 
Quick script to query certificate information for all domains hosted on Imperva Cloud WAF.  

***Note:*** Script execution requires a Mac or *nix system due to dependecy on OpenSSL native binary.

## Installation
Update `config.js` to specify your API ID, API Key, and account ID.

`account_id` - _(required)_ the account ID associated with your Imperva Cloud WAF account  

`api_id` - _(required)_ the API ID of a Cloud WAF user with READ permissions 

`api_key` - _(required)_ the API Key for the Cloud WAF user


Execute the following commands.
```
npm install
node index.js
```

## Example

```
$ node index.js 
POST /api/prov/v1/sites/list
Status: 200
Requesting SSL Certificate for www.imperva.com
Requesting SSL Certificate for my.imperva.com
Writing results to account-12345.json
Writing results to account-12345.csv

```
