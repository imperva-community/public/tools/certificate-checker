const request = require("request-promise-native");
const fs = require("fs");
const jsonexport = require('jsonexport');
const execSync = require('child_process').execSync;
const config = require('./config');

const ACCOUNT_ID = config.account_id;
const API_ID = config.api_id;
const API_KEY = config.api_key;
const AUTH_PARAMS = "?api_id=" + API_ID + "&api_key=" + API_KEY + "&account_id=" + ACCOUNT_ID;

const URL = "https://my.incapsula.com";
const LIST_SITES = URL + "/api/prov/v1/sites/list" + AUTH_PARAMS;
const GET_SITE_STATUS = URL + "/api/prov/v1/sites/status" + AUTH_PARAMS;

let results = [];
main();

/**
 * Iterates all sites in a given account and outputs a CSV and JSON file with the status of the SSL certificates for each domain.
 * If a custom certificate has been uploaded, the script will manually query the domain and pull the certificate creation dates.
 * @returns {Promise<void>}
 */
async function main() {
    let sites = [];
    let res = await getSites(0);
    sites = sites.concat(res);
    if(res.length === 100) {
        // Need to query for additional sites
        let i = 1;
        while(res.length === 100) {
            res = await getSites(i);
            sites = sites.concat(res);
            i++;
        }
    }

    sites.forEach((siteRes) => {
        let site = {
            "site_id": siteRes.site_id,
            "domain": siteRes.domain,
            "account_id": siteRes.account_id
        };
        if(siteRes.ssl) {
            site.ssl = siteRes.ssl;
            if(site.ssl.custom_certificate && site.ssl.custom_certificate.active) {
                try {
                    let date = new Date(site.ssl.custom_certificate.expirationDate);
                    site.ssl.custom_certificate.expirationDateISO = date.toISOString();
                    console.log(`Requesting SSL Certificate for ${site.domain}`);
                    let openssl = execSync(
                        `openssl s_client -servername ${site.domain} -connect ${site.domain}:443 2>/dev/null | openssl x509 -noout -dates`,
                        {encoding: 'utf-8'}).split("\n");
                    site.ssl.custom_certificate.notBefore = openssl[0].split("=")[1];
                    site.ssl.custom_certificate.notAfter = openssl[1].split("=")[1];
                }
                catch(e) {
                    console.log(`Received error ${e.message} when attempting to request custom SSL certificate for ${site.domain}`);
                }
            }
        }
        results.push(site);
    });

    // Write the output to CSV
    console.log(`Writing results to account-${ACCOUNT_ID}.json`);
    fs.writeFileSync(`./account-${ACCOUNT_ID}.json`, JSON.stringify(results, null, 3));

    // Write the output to JSON
    jsonexport(results, (err, csv) => {
        if(!err) {
            console.log(`Writing results to account-${ACCOUNT_ID}.csv`);
            fs.writeFileSync(`./account-${ACCOUNT_ID}.csv`, csv);
        }
        else {
            console.log(err.message);
        }

    });
}

/**
 * Queries the Imperva Cloud WAF API and returns an array of all sites.  The first 100 sites will be retrieved by the API by default.
 * Additional pages can be queried by passing in a positive integer for pageNumber.
 * @param pageNumber: number
 * @returns {Promise<Array<Object>>}
 */
async function getSites(pageNumber = 0, debug = false) {
    console.log(`POST /api/prov/v1/sites/list?page_num=${pageNumber}`);
    try {
        let response = await request.post({url: `${LIST_SITES}&page_size=100&page_num=${pageNumber}`, json: true, body: ""});
        console.log(`Cloud WAF Response Status: ${response.res}`);
        if(debug) {
            fs.writeFileSync(`./account-${ACCOUNT_ID}-${pageNumber}-debug.json`, JSON.stringify(response, null, 3));
        }
        if(response.res === 0) {
            console.log(`Successfully retrieved ${response.sites.length} sites`);
            return response.sites;
        }
        else {
            console.log(`FATAL - Failed to fetch site data with error ${response.res_message}`);
            process.exit(1);
        }
    }
    catch (e) {
        console.log(`FATAL - Failed to fetch site data with error ${e.message}`);
        process.exit(1);
    }
}
